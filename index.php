<?php
function twoNumberCalculate($data)
{
    $number1 = isset($data['num1']) ? $data['num1'] : null;
    $number2 = isset($data['num2']) ? $data['num2'] : null;
    $calculate = isset($data['calculate']) ? $data['calculate'] : null;

    switch ($calculate) {
        case  '+':
            $result = $number1 + $number2;
            break;
        case '-':
            $result = $number1 - $number2;
            break;
        case '*':
            $result = $number1 * $number2;
            break;
        case '/':
            $result = $number1 / $number2;
            break;
        default:
            return false;
    }

    return $result;
}

$result = twoNumberCalculate($_POST);

print_r($result)

?>
<html>

<header>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</header>
<!-- create table -->
<body>
<div class="container mt-5">
    <div class="card">
        <div class="card-header">Calculator</div>
        <div class="card-body">
            <div class="">
                <form action="../view/index.php" method="post" class="px-4 py-3">
                    <div class="form-group">
                        <label for="">Number1</label>
                        <input type="text" class="form-control" name="num1"/>
                    </div>
                    <div class="form-group">
                        <label for="">Number2</label>
                        <input type="text" class="form-control" name="num2"/>
                    </div>
                    <input class="btn btn-danger" value="+" type="submit" name="calculate">
                    <input class="btn btn-success" value="-" type="submit" name="calculate">
                    <input type="submit" class="btn btn-primary" value="*" name="calculate">
                    <input type="submit" class="btn btn-primary" value="/" name="calculate">
                </form>

                <div>
                    <label for="">Result : <span><?php echo $result;?></span></label>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
